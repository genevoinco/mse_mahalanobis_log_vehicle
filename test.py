from __future__ import absolute_import
import os
import errno
import os.path
from os import path

import shutil
def mkdir_if_missing(dir_path):
    try:
        os.makedirs(dir_path)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

all_dir = os.listdir(os.path.join('data', 'atlantic'));
mkdir_if_missing(os.path.join('data', 'atlantic','gallery'));
mkdir_if_missing(os.path.join('data', 'atlantic','train'));
mkdir_if_missing(os.path.join('data', 'atlantic','query'));
cnt = 0
for dir in all_dir:
                if(int(dir)%2 == 0):
                  if (cnt < 0.8 * len(all_dir)):
                    print(dir + " paire add to query and gallery")
                    shutil.move(os.path.join('data', 'atlantic', dir),
                                os.path.join('data', 'atlantic', 'gallery', dir))
                else:
                    if(cnt < 0.8*len(all_dir)):
                        print(dir + " impaire add to train")
                        shutil.move(os.path.join('data', 'atlantic', dir),
                                    os.path.join('data', 'atlantic', 'train', dir))
                    else:
                        break
                cnt = cnt + 1




